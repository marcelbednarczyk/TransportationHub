﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    interface IPassengerTransport : IVehicle
    {
        int MaxPassengers { get; set; }
    }
}
