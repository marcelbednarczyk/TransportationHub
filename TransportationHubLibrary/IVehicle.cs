﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    public interface IVehicle
    {
        string MakeAndModel { get; set; }
        string LicensePlate { get; set; }
        double? GasUsage { get; set; }
        double PricePerKilometre { get; set; }
        int TotalKilometres { get; set; }

        double CalculateConsumedFuel(int distance);
    }
}
