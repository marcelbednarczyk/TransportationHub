﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    public class TransportationHub
    {
        public List<Ride> CurrentRides { get; set; }
        public List<Ride> CompletedRides { get; set; }
        public List<IVehicle> AvailableVehicles { get; set; }
        public List<IVehicle> BusyVehicles { get; set; }
    }
}
