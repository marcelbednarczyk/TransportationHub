﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    public class Truck : IFreighter
    {
        public string LicensePlate { get; set; }
        public string MakeAndModel { get; set; }
        public double? GasUsage { get; set; }
        public double PricePerKilometre { get; set; }
        public int TotalKilometres { get; set; }
        public int MaxWeight { get; set; }
        public double MaxVolume { get; set; }

        public Truck()
        {
            LicensePlate = "";
            MakeAndModel = null;
            GasUsage = null;
            PricePerKilometre = 0;
            TotalKilometres = 0;
            MaxWeight = 0;
            MaxVolume = 0;
        }
        public Truck(string licensePlate, double pricePreKm, int totalKm, int maxWeight, double maxVolume, string makeAndModel = null, double? gasUsage = null)
        {
            LicensePlate = licensePlate;
            MakeAndModel = makeAndModel;
            GasUsage = gasUsage;
            PricePerKilometre = pricePreKm;
            TotalKilometres = totalKm;
            MaxWeight = maxWeight;
            MaxVolume = maxVolume;
        }

        public Truck(Van van)
        {
            LicensePlate = van.LicensePlate;
            MakeAndModel = van.MakeAndModel;
            GasUsage = van.GasUsage;
            PricePerKilometre = van.PricePerKilometre;
            TotalKilometres = van.TotalKilometres;
            MaxWeight = van.MaxWeight;
            MaxVolume = van.MaxVolume;
        }

        public double CalculateConsumedFuel(int distance)
        {
            if (GasUsage.HasValue)
            {
                return distance * GasUsage.Value / 100;
            }
            return 0;
        }
    }
}
