﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TransportationHubLibrary
{
    public class Ride : EntityValidator
    {
        public int ID { get; set; }
        public string VehicleLicensePlate { get; set; }
        public int People { get; set; }
        public double Volume { get; set; }
        public int Weight { get; set; }
        public double Price { get; set; }
        public double StartPrice { get; set; }
        [Required(ErrorMessage = "Distance is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid Distance")]
        public int Distance { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Status { get; set; }

        public Ride()
        {
            VehicleLicensePlate = null;
            People = 0;
            Volume = 0.0;
            Weight = 0;
            Price = 0.0;
            StartPrice = 0.0;
            Distance = 0;
            StartTime = new DateTime();
            EndTime = new DateTime();
            Status = "";
        }

        public Ride(string licensePlate, int distance, int people, double volume, int weight)
        {
            VehicleLicensePlate = licensePlate;
            People = people;
            Volume = volume;
            Weight = weight;
            StartPrice = 2;
            Distance = distance;
            StartTime = DateTime.Now;
            EndTime = null;
            Price = CalculatePrice();
            Status = "In Progress";
        }

        public Ride(Ride ride)
        {
            VehicleLicensePlate = ride.VehicleLicensePlate;
            People = ride.People;
            Volume = ride.Volume;
            Weight = ride.Weight;
            StartPrice = ride.StartPrice;
            Distance = ride.Distance;
            StartTime = ride.StartTime;
            EndTime = ride.EndTime;
            Price = CalculatePrice();
            Status = ride.Status;
        }

        public double CalculatePrice()
        {
            return CalculatePrice(Distance, SqliteDataAccess.LoadPriceOfVehicle(VehicleLicensePlate), StartPrice);
        }

        public static double CalculatePrice(int distance, double price, double startPrice)
        {
            return distance * price + startPrice;
        }

        public void Finish()
        {
            if(Status == "In Progress")
            {
                EndTime = DateTime.Now;
                Status = "Finished";
            }
        }
    }
}
