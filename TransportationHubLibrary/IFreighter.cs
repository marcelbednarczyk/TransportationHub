﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    interface IFreighter : IVehicle
    {
        int MaxWeight { get; set; }
        double MaxVolume { get; set; }
    }
}
