﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;

namespace TransportationHubLibrary
{
    public class SqliteDataAccess
    {
        

        public static List<string> LoadLicensePlates()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<string>("SELECT LicensePlate FROM (SELECT  LicensePlate FROM Car UNION ALL SELECT  LicensePlate FROM Van UNION ALL SELECT  LicensePlate FROM Truck) AS Vehicle", new DynamicParameters());
                return output.ToList();
            }
        }

        public static bool LicensePlateExist(string licensePlate)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<string>($"SELECT LicensePlate FROM (SELECT  LicensePlate FROM Car UNION ALL SELECT  LicensePlate FROM Van UNION ALL SELECT  LicensePlate FROM Truck) AS Vehicle WHERE LicensePlate LIKE \"{licensePlate}\"", new DynamicParameters());
                return output.ToList().Count > 0;
            }
        }

        public static void SaveVehicle(Car vehicle)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("INSERT INTO Car (LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers) values (@LicensePlate, @MakeAndModel, @GasUsage, @PricePerKilometre, @TotalKilometres, @MaxPassengers)", vehicle);
            }
        }

        public static void UpdateVehicle(Car vehicle)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute($"UPDATE Car SET LicensePlate = @LicensePlate, MakeAndModel = @MakeAndModel, GasUsage = @GasUsage, PricePerKilometre = @PricePerKilometre, TotalKilometres = @TotalKilometres, MaxPassengers = @MaxPassengers WHERE LicensePlate = @LicensePlate", vehicle);
            }
        }

        public static void SaveVehicle(Van vehicle)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("INSERT INTO Van (LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume) values (@LicensePlate, @MakeAndModel, @GasUsage, @PricePerKilometre, @TotalKilometres, @MaxPassengers, @MaxWeight, @MaxVolume)", vehicle);
            }
        }

        public static List<Van> LoadVehicles()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Van>("SELECT * FROM (SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 AS MaxWeight, 0 AS MaxVolume FROM Car UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 AS MaxPassengers, MaxWeight, MaxVolume FROM Truck) AS Vehicle", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Van> LoadAvailableVehicles()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Van>("SELECT * FROM (SELECT * FROM (SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 AS MaxWeight, 0 AS MaxVolume FROM Car UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 AS MaxPassengers, MaxWeight, MaxVolume FROM Truck)) AS Vehicles WHERE LicensePlate NOT IN(SELECT LicensePlate FROM(SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 AS MaxWeight, 0 AS MaxVolume FROM Car UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 AS MaxPassengers, MaxWeight, MaxVolume FROM Truck) AS Vehicle LEFT JOIN Ride ON LicensePlate = VehicleLicensePlate WHERE Status NOT LIKE \"Finished\")", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Van> LoadUnavailableVehicles()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Van>("SELECT LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM(SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 AS MaxWeight, 0 AS MaxVolume FROM Car UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 AS MaxPassengers, MaxWeight, MaxVolume FROM Truck) AS Vehicle LEFT JOIN Ride ON LicensePlate = VehicleLicensePlate WHERE Status NOT LIKE \"Finished\"", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Ride> LoadRides()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Ride>("SELECT * FROM Ride", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Ride> LoadRidesInProgress()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Ride>("SELECT * FROM Ride WHERE Status LIKE \"In Progress\"", new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<Ride> LoadFinishedRides()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Ride>("SELECT * FROM Ride WHERE Status LIKE \"Finished\"", new DynamicParameters());
                return output.ToList();
            }
        }

        public static Ride LoadRide(int ID)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Ride>($"SELECT * FROM Ride WHERE ID={ID}", new DynamicParameters());
                if (output.Any())
                {
                    return output.ToList()[0];
                }
                return null;
            }
        }

        public static Van LoadVehicle(string licensePlate)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<Van>($"SELECT * FROM (SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 AS MaxWeight, 0 AS MaxVolume FROM Car UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 AS MaxPassengers, MaxWeight, MaxVolume FROM Truck) AS Vehicle WHERE LicensePlate LIKE \"{licensePlate}\"", new DynamicParameters());
                if (output.Any())
                {
                    return output.ToList()[0];
                }
                return null;
            }
        }

        public static double LoadPriceOfVehicle(string licensePlate)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<double>($"SELECT PricePerKilometre FROM (SELECT LicensePlate, PricePerKilometre FROM Car UNION ALL SELECT LicensePlate, PricePerKilometre FROM Van UNION ALL SELECT LicensePlate, PricePerKilometre FROM Truck) AS Vehicle WHERE LicensePlate LIKE \"{licensePlate}\"", new DynamicParameters());
                if (output.Any())
                {
                    return output.ToList()[0];
                }
                return 0.0;
            }
        }

        public static string FindVehicle(int people, int weight, double volume)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<string>($"SELECT * FROM (SELECT LicensePlate FROM (SELECT LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 as MaxWeight, 0 as MaxVolume FROM Car UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT  LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 as MaxPassengers, MaxWeight, MaxVolume FROM Truck) AS Vehicle LEFT JOIN Ride ON LicensePlate = VehicleLicensePlate WHERE MaxPassengers >= {people} AND MaxWeight >= {weight} AND MaxVolume >= {volume}) WHERE LicensePlate NOT IN(SELECT LicensePlate FROM (SELECT LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, 0 as MaxWeight, 0 as MaxVolume FROM Car UNION ALL SELECT LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxPassengers, MaxWeight, MaxVolume FROM Van UNION ALL SELECT LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, 0 as MaxPassengers, MaxWeight, MaxVolume FROM Truck) AS Vehicle LEFT JOIN Ride ON LicensePlate = VehicleLicensePlate WHERE MaxPassengers >= {people} AND MaxWeight >= {weight} AND MaxVolume >= {volume} AND Status NOT LIKE \"Finished\")", new DynamicParameters());
                if (output.Any())
                {
                    return output.ToList()[0];
                }
                return null;
            }
        }

        public static void UpdateVehicle(Van vehicle)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute($"UPDATE Van SET LicensePlate = @LicensePlate, MakeAndModel = @MakeAndModel, GasUsage = @GasUsage, PricePerKilometre = @PricePerKilometre, TotalKilometres = @TotalKilometres, MaxPassengers = @MaxPassengers, MaxWeight = @MaxWeight, MaxVolume = @MaxVolume WHERE LicensePlate = @LicensePlate", vehicle);
            }
        }

        public static void SaveVehicle(Truck vehicle)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("INSERT INTO Truck (LicensePlate, MakeAndModel, GasUsage, PricePerKilometre, TotalKilometres, MaxWeight, MaxVolume) values (@LicensePlate, @MakeAndModel, @GasUsage, @PricePerKilometre, @TotalKilometres, @MaxWeight, @MaxVolume)", vehicle);
            }
        }

        public static void UpdateVehicle(Truck vehicle)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute($"UPDATE Truck SET LicensePlate = @LicensePlate, MakeAndModel = @MakeAndModel, GasUsage = @GasUsage, PricePerKilometre = @PricePerKilometre, TotalKilometres = @TotalKilometres, MaxWeight = @MaxWeight, MaxVolume = @MaxVolume WHERE LicensePlate = @LicensePlate", vehicle);
            }
        }

        public static void SaveRide(Ride ride)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("INSERT INTO Ride (VehicleLicensePlate, People, Volume, Weight, Price, StartPrice, Distance, StartTime, EndTime, Status) values (@VehicleLicensePlate, @People, @Volume, @Weight, @Price, @StartPrice, @Distance, @StartTime, @EndTime, @Status)", ride);
            }
        }

        public static void UpdateRide(Ride ride)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute($"UPDATE Ride SET VehicleLicensePlate = @VehicleLicensePlate, People = @People, Volume = @Volume, Weight = @Weight, Price = @Price, StartPrice = @StartPrice, Distance = @Distance, StartTime = @StartTime, EndTime = @EndTime, Status = @Status WHERE ID = @ID", ride);
            }
        }

        private static string LoadConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
    }
}
