﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    public class Car : IPassengerTransport
    {
        public string LicensePlate { get; set; }
        public string MakeAndModel { get; set; }
        public double? GasUsage { get; set; }
        public double PricePerKilometre { get; set; }
        public int TotalKilometres { get; set; }
        public int MaxPassengers { get; set; }

        public Car()
        {
            LicensePlate = "";
            MakeAndModel = null;
            GasUsage = null;
            PricePerKilometre = 0;
            TotalKilometres = 0;
            MaxPassengers = 0;
        }

        public Car(string licensePlate, double pricePreKm, int totalKm, int maxPassengers, string makeAndModel = null, double? gasUsage = null)
        {
            LicensePlate = licensePlate;
            MakeAndModel = makeAndModel;
            GasUsage = gasUsage;
            PricePerKilometre = pricePreKm;
            TotalKilometres = totalKm;
            MaxPassengers = maxPassengers;
        }

        public Car(Van van)
        {
            LicensePlate = van.LicensePlate;
            MakeAndModel = van.MakeAndModel;
            GasUsage = van.GasUsage;
            PricePerKilometre = van.PricePerKilometre;
            TotalKilometres = van.TotalKilometres;
            MaxPassengers = van.MaxPassengers;
        }

        public double CalculateConsumedFuel(int distance)
        {
            if (GasUsage.HasValue)
            {
                return distance * GasUsage.Value / 100;
            }
            return 0;
        }
    }
}
