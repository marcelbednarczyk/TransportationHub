﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportationHubLibrary
{
    public class Van : IPassengerTransport, IFreighter
    {
        public string LicensePlate { get; set; }
        public string MakeAndModel { get; set; }
        public double? GasUsage { get; set; }
        public double PricePerKilometre { get; set; }
        public int TotalKilometres { get; set; }
        public int MaxPassengers { get; set; }
        public int MaxWeight { get; set; }
        public double MaxVolume { get; set; }

        public Van()
        {
            LicensePlate = "";
            MakeAndModel = null;
            GasUsage = null;
            PricePerKilometre = 0;
            TotalKilometres = 0;
            MaxPassengers = 0;
            MaxWeight = 0;
            MaxVolume = 0;
        }
        public Van(string licensePlate, double pricePreKm, int totalKm, int maxPassengers, int maxWeight, double maxVolume, string makeAndModel = null, double? gasUsage = null)
        {
            LicensePlate = licensePlate;
            MakeAndModel = makeAndModel;
            GasUsage = gasUsage;
            PricePerKilometre = pricePreKm;
            TotalKilometres = totalKm;
            MaxPassengers = maxPassengers;
            MaxWeight = maxWeight;
            MaxVolume = maxVolume;
        }

        public double CalculateConsumedFuel(int distance)
        {
            if (GasUsage.HasValue)
            {
                return distance * GasUsage.Value / 100;
            }
            return 0;
        }
    }
}
