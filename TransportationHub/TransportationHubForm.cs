﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TransportationHubLibrary;
using TransportationHub;

namespace Transportation_Hub
{
    public partial class TransportationHubForm : Form
    {
        private List<Van> availVehicles;
        private List<Van> vehicles;
        private List<Ride> rides;
        private List<Ride> finishedRides;

        public TransportationHubForm()
        {
            InitializeComponent();
            LoadVehiclesList();
            LoadRidesList();
            listAvailVehiclesListView.ListViewItemSorter = new ListViewItemComparerAsc(0);
            listVehiclesListView.ListViewItemSorter = new ListViewItemComparerAsc(0);
            completedRidesListView.ListViewItemSorter = new ListViewItemComparerAsc(0);
            ridesListView.ListViewItemSorter = new ListViewItemComparerAsc(0);
        }

        private void btn_add_new_Click(object sender, EventArgs e)
        {
            NewVehicleForm form = new NewVehicleForm(this);
            Enabled = false;
            form.Show();
        }

        private void LoadVehiclesList()
        {
            vehicles = SqliteDataAccess.LoadUnavailableVehicles();
            availVehicles = SqliteDataAccess.LoadAvailableVehicles();

            WireUpVehiclesList();
            WireUpAvailVehiclesList();
        }

        private void WireUpAvailVehiclesList()
        {
            listAvailVehiclesListView.Items.Clear();
            foreach (var vehicle in availVehicles)
            {
                ListViewItem listitem = new ListViewItem(vehicle.LicensePlate);
                listitem.SubItems.Add(vehicle.MakeAndModel);
                listitem.SubItems.Add(vehicle.PricePerKilometre.ToString());
                if (vehicle.MaxPassengers <= 0)
                {
                    listitem.SubItems.Add("Truck");
                }
                else if (vehicle.MaxWeight <= 0)
                {
                    listitem.SubItems.Add("Car");
                }
                else
                {
                    listitem.SubItems.Add("Van");
                }
                listAvailVehiclesListView.Items.Add(listitem);
            }
        }

        private void WireUpVehiclesList()
        {
            listVehiclesListView.Items.Clear();
            foreach (var vehicle in vehicles)
            {
                ListViewItem listitem = new ListViewItem(vehicle.LicensePlate);
                listitem.SubItems.Add(vehicle.MakeAndModel);
                listitem.SubItems.Add(vehicle.PricePerKilometre.ToString());
                if(vehicle.MaxPassengers <= 0)
                {
                    listitem.SubItems.Add("Truck");
                }
                else if (vehicle.MaxWeight <= 0)
                {
                    listitem.SubItems.Add("Car");
                }
                else
                {
                    listitem.SubItems.Add("Van");
                }
                listVehiclesListView.Items.Add(listitem);
            }
        }

        private void LoadRidesList()
        {
            rides = SqliteDataAccess.LoadRidesInProgress();
            finishedRides = SqliteDataAccess.LoadFinishedRides();

            WireUpRidesList();
            WireUpFinishedRidesList();
        }

        private void WireUpRidesList()
        {
            ridesListView.Items.Clear();
            foreach (var ride in rides)
            {
                ListViewItem listitem = new ListViewItem(ride.ID.ToString());
                listitem.SubItems.Add(ride.VehicleLicensePlate);
                listitem.SubItems.Add(ride.StartTime.ToString());
                listitem.SubItems.Add(ride.Status.ToString());
                ridesListView.Items.Add(listitem);
            }
        }

        private void WireUpFinishedRidesList()
        {
            completedRidesListView.Items.Clear();
            foreach (var ride in finishedRides)
            {
                ListViewItem listitem = new ListViewItem(ride.ID.ToString());
                listitem.SubItems.Add(ride.VehicleLicensePlate);
                listitem.SubItems.Add(ride.StartTime.ToString());
                listitem.SubItems.Add(ride.EndTime.ToString());
                listitem.SubItems.Add(ride.Distance.ToString());
                listitem.SubItems.Add(ride.Price.ToString());
                listitem.SubItems.Add(ride.Status.ToString());
                completedRidesListView.Items.Add(listitem);
            }
        }

        private void listVehiclesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if(listAvailVehiclesListView.ListViewItemSorter.GetType() == new ListViewItemComparerAsc(e.Column).GetType())
            {
                listAvailVehiclesListView.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
            }
            else
            {
                listAvailVehiclesListView.ListViewItemSorter = new ListViewItemComparerAsc(e.Column);
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            LoadVehiclesList();
            LoadRidesList();
        }

        private void listVehiclesListView_ItemActivate(object sender, EventArgs e)
        {
            
            NewVehicleForm form = new NewVehicleForm(this, listAvailVehiclesListView.FocusedItem.Text);
            Enabled = false;
            form.Show();
        }

        private void VehiclesForm_Activated(object sender, EventArgs e)
        {
            LoadVehiclesList();
            LoadRidesList();
        }

        private void newRideButton_Click(object sender, EventArgs e)
        {
            NewRideForm form = new NewRideForm(this);
            Enabled = false;
            form.Show();
        }

        private void completedRidesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (completedRidesListView.ListViewItemSorter.GetType() == new ListViewItemComparerAsc(e.Column).GetType())
            {
                completedRidesListView.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
            }
            else
            {
                completedRidesListView.ListViewItemSorter = new ListViewItemComparerAsc(e.Column);
            }
        }

        private void ridesListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (ridesListView.ListViewItemSorter.GetType() == new ListViewItemComparerAsc(e.Column).GetType())
            {
                ridesListView.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
            }
            else
            {
                ridesListView.ListViewItemSorter = new ListViewItemComparerAsc(e.Column);
            }
        }

        private void listVehiclesListView_ColumnClick_1(object sender, ColumnClickEventArgs e)
        {
            if (listVehiclesListView.ListViewItemSorter.GetType() == new ListViewItemComparerAsc(e.Column).GetType())
            {
                listVehiclesListView.ListViewItemSorter = new ListViewItemComparerDesc(e.Column);
            }
            else
            {
                listVehiclesListView.ListViewItemSorter = new ListViewItemComparerAsc(e.Column);
            }
        }

        private void ridesListView_ItemActivate(object sender, EventArgs e)
        {
            int.TryParse(ridesListView.FocusedItem.Text, out int id);
            FinishRideForm form = new FinishRideForm(this, id);
            Enabled = false;
            form.Show();
        }

        private void completedRidesListView_ItemActivate(object sender, EventArgs e)
        {
            int.TryParse(completedRidesListView.FocusedItem.Text, out int id);
            FinishRideForm form = new FinishRideForm(this, id);
            Enabled = false;
            form.Show();
        }

        private void listVehiclesListView_ItemActivate_1(object sender, EventArgs e)
        {
            NewVehicleForm form = new NewVehicleForm(this, listVehiclesListView.FocusedItem.Text);
            Enabled = false;
            form.Show();
        }
    }
}
