﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace TransportationHub
{
    class ListViewItemComparerDesc : IComparer
    {
        private int col;
        public ListViewItemComparerDesc()
        {
            col = 0;
        }
        public ListViewItemComparerDesc(int column)
        {
            col = column;
        }

        public int Compare(object x, object y)
        {
            if (int.TryParse(((ListViewItem)x).SubItems[col].Text, out int _x) && int.TryParse(((ListViewItem)y).SubItems[col].Text, out int _y))
            {
                return _x < _y ? 1 : -1;
            }
            return String.Compare(((ListViewItem)y).SubItems[col].Text, ((ListViewItem)x).SubItems[col].Text);
        }
    }
}
