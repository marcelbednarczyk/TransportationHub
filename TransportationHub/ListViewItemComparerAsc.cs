﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace TransportationHub
{
    class ListViewItemComparerAsc : IComparer
    {
        private int col;
        public ListViewItemComparerAsc()
        {
            col = 0;
        }
        public ListViewItemComparerAsc(int column)
        {
            col = column;
        }
        public int Compare(object x, object y)
        {
            if (int.TryParse(((ListViewItem)x).SubItems[col].Text, out int _x) && int.TryParse(((ListViewItem)y).SubItems[col].Text, out int _y))
            {
                return _x > _y ? 1 : -1;
            }
            return String.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
        }
    }
}
