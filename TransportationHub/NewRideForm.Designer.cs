﻿
namespace TransportationHub
{
    partial class NewRideForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label distanceLabel1;
            System.Windows.Forms.Label peopleLabel1;
            System.Windows.Forms.Label volumeLabel1;
            System.Windows.Forms.Label weightLabel1;
            this.button1 = new System.Windows.Forms.Button();
            this.distanceNumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.rideBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.peopleNumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.volumeNumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.weightNumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            distanceLabel1 = new System.Windows.Forms.Label();
            peopleLabel1 = new System.Windows.Forms.Label();
            volumeLabel1 = new System.Windows.Forms.Label();
            weightLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.distanceNumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleNumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeNumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // distanceLabel1
            // 
            distanceLabel1.AutoSize = true;
            distanceLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            distanceLabel1.Location = new System.Drawing.Point(26, 26);
            distanceLabel1.Name = "distanceLabel1";
            distanceLabel1.Size = new System.Drawing.Size(81, 20);
            distanceLabel1.TabIndex = 18;
            distanceLabel1.Text = "Distance:";
            // 
            // peopleLabel1
            // 
            peopleLabel1.AutoSize = true;
            peopleLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            peopleLabel1.Location = new System.Drawing.Point(42, 62);
            peopleLabel1.Name = "peopleLabel1";
            peopleLabel1.Size = new System.Drawing.Size(65, 20);
            peopleLabel1.TabIndex = 20;
            peopleLabel1.Text = "People:";
            // 
            // volumeLabel1
            // 
            volumeLabel1.AutoSize = true;
            volumeLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            volumeLabel1.Location = new System.Drawing.Point(37, 98);
            volumeLabel1.Name = "volumeLabel1";
            volumeLabel1.Size = new System.Drawing.Size(70, 20);
            volumeLabel1.TabIndex = 22;
            volumeLabel1.Text = "Volume:";
            // 
            // weightLabel1
            // 
            weightLabel1.AutoSize = true;
            weightLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            weightLabel1.Location = new System.Drawing.Point(41, 134);
            weightLabel1.Name = "weightLabel1";
            weightLabel1.Size = new System.Drawing.Size(66, 20);
            weightLabel1.TabIndex = 24;
            weightLabel1.Text = "Weight:";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(154, 165);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 30);
            this.button1.TabIndex = 17;
            this.button1.Text = "Find";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // distanceNumericUpDown1
            // 
            this.distanceNumericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.rideBindingSource, "Distance", true));
            this.distanceNumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.distanceNumericUpDown1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.distanceNumericUpDown1.Location = new System.Drawing.Point(116, 24);
            this.distanceNumericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.distanceNumericUpDown1.Name = "distanceNumericUpDown1";
            this.distanceNumericUpDown1.Size = new System.Drawing.Size(120, 27);
            this.distanceNumericUpDown1.TabIndex = 19;
            // 
            // rideBindingSource
            // 
            this.rideBindingSource.DataSource = typeof(TransportationHubLibrary.Ride);
            // 
            // peopleNumericUpDown1
            // 
            this.peopleNumericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.rideBindingSource, "People", true));
            this.peopleNumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.peopleNumericUpDown1.Location = new System.Drawing.Point(116, 60);
            this.peopleNumericUpDown1.Name = "peopleNumericUpDown1";
            this.peopleNumericUpDown1.Size = new System.Drawing.Size(120, 27);
            this.peopleNumericUpDown1.TabIndex = 21;
            // 
            // volumeNumericUpDown1
            // 
            this.volumeNumericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.rideBindingSource, "Volume", true));
            this.volumeNumericUpDown1.DecimalPlaces = 2;
            this.volumeNumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.volumeNumericUpDown1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.volumeNumericUpDown1.Location = new System.Drawing.Point(116, 96);
            this.volumeNumericUpDown1.Name = "volumeNumericUpDown1";
            this.volumeNumericUpDown1.Size = new System.Drawing.Size(120, 27);
            this.volumeNumericUpDown1.TabIndex = 23;
            // 
            // weightNumericUpDown1
            // 
            this.weightNumericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.rideBindingSource, "Weight", true));
            this.weightNumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.weightNumericUpDown1.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.weightNumericUpDown1.Location = new System.Drawing.Point(116, 132);
            this.weightNumericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.weightNumericUpDown1.Name = "weightNumericUpDown1";
            this.weightNumericUpDown1.Size = new System.Drawing.Size(120, 27);
            this.weightNumericUpDown1.TabIndex = 25;
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            this.errorProvider.DataSource = this.rideBindingSource;
            // 
            // NewRideForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 229);
            this.Controls.Add(distanceLabel1);
            this.Controls.Add(this.distanceNumericUpDown1);
            this.Controls.Add(peopleLabel1);
            this.Controls.Add(this.peopleNumericUpDown1);
            this.Controls.Add(volumeLabel1);
            this.Controls.Add(this.volumeNumericUpDown1);
            this.Controls.Add(weightLabel1);
            this.Controls.Add(this.weightNumericUpDown1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "NewRideForm";
            this.Text = "Add New Ride";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewRideForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.distanceNumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rideBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peopleNumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.volumeNumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.weightNumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.BindingSource rideBindingSource;
        private System.Windows.Forms.NumericUpDown distanceNumericUpDown1;
        private System.Windows.Forms.NumericUpDown peopleNumericUpDown1;
        private System.Windows.Forms.NumericUpDown volumeNumericUpDown1;
        private System.Windows.Forms.NumericUpDown weightNumericUpDown1;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}