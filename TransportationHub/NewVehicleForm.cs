﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TransportationHubLibrary;

namespace Transportation_Hub
{
    public partial class NewVehicleForm : Form
    {
        private IVehicle _vehicle;
        private bool _edit = false;
        private Form _frm;

        public NewVehicleForm(TransportationHubForm senderForm)
        {
            _frm = senderForm;
            _edit = false;
            InitializeComponent();
            Text = "Add New Vehicle";
        }

        public NewVehicleForm(TransportationHubForm senderForm, string licensePlate)
        {
            _frm = senderForm;
            _edit = true;
            InitializeComponent();
            Text = "Edit Vehicle";

            licensePlateTextBox.Text = licensePlate;
            Van loadedVehicle = SqliteDataAccess.LoadVehicle(licensePlate);
            if (loadedVehicle.MaxWeight <= 0)
            {
                typeOfVehicleComboBox.SelectedIndex = 1;
            }
            else if (loadedVehicle.MaxPassengers > 0)
            {
                typeOfVehicleComboBox.SelectedIndex = 2;
            }
            else
            {
                typeOfVehicleComboBox.SelectedIndex = 3;
            }
            makeAndModelTextBox.Text = loadedVehicle.MakeAndModel;
            gasUsageNumericUpDown.Value = (decimal)loadedVehicle.GasUsage.GetValueOrDefault();
            priceNumericUpDown.Value = (decimal)loadedVehicle.PricePerKilometre;
            totalKmNumericUpDown.Value = loadedVehicle.TotalKilometres;
            maxPassengersNumericUpDown.Value = loadedVehicle.MaxPassengers;
            maxWeightNumericUpDown.Value = loadedVehicle.MaxWeight;
            maxVolumeNumericUpDown.Value = (decimal)loadedVehicle.MaxVolume;
            typeOfVehicleComboBox.Enabled = false;
            licensePlateTextBox.Enabled = false;
        }

        private void SaveNewVehicle()
        {
            switch (typeOfVehicleComboBox.SelectedItem)
            {
                case "Car":
                    _vehicle = new Car(licensePlateTextBox.Text, 
                                      (double)priceNumericUpDown.Value, 
                                      (int)totalKmNumericUpDown.Value, 
                                      (int)maxPassengersNumericUpDown.Value,
                                      makeAndModel: makeAndModelTextBox.Text,
                                      gasUsage: (double)gasUsageNumericUpDown.Value);
                    if (_edit)
                    {
                        SqliteDataAccess.UpdateVehicle(_vehicle as Car);
                    }
                    else
                    {
                        SqliteDataAccess.SaveVehicle(_vehicle as Car);
                    }
                    break;
                case "Van":
                    _vehicle = new Van(licensePlateTextBox.Text,
                                      (double)priceNumericUpDown.Value,
                                      (int)totalKmNumericUpDown.Value,
                                      (int)maxPassengersNumericUpDown.Value,
                                      (int)maxWeightNumericUpDown.Value,
                                      (double)maxVolumeNumericUpDown.Value,
                                      makeAndModel: makeAndModelTextBox.Text,
                                      gasUsage: (double)gasUsageNumericUpDown.Value);
                    if (_edit)
                    {
                        SqliteDataAccess.UpdateVehicle(_vehicle as Van);
                    }
                    else
                    {
                        SqliteDataAccess.SaveVehicle(_vehicle as Van);
                    }
                    break;
                case "Truck":
                    _vehicle = new Truck(licensePlateTextBox.Text,
                                      (double)priceNumericUpDown.Value,
                                      (int)totalKmNumericUpDown.Value,
                                      (int)maxWeightNumericUpDown.Value,
                                      (double)maxVolumeNumericUpDown.Value,
                                      makeAndModel: makeAndModelTextBox.Text,
                                      gasUsage: (double)gasUsageNumericUpDown.Value);
                    if (_edit)
                    {
                        SqliteDataAccess.UpdateVehicle(_vehicle as Truck);
                    }
                    else
                    {
                        SqliteDataAccess.SaveVehicle(_vehicle as Truck);
                    }
                    break;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (IsAllValid())
            {
                SaveNewVehicle();
                Close();
            }
        }

        private void typeOfVehicleComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            switch (typeOfVehicleComboBox.SelectedItem.ToString())
            {
                case "Car":
                    maxPassengersNumericUpDown.Enabled = true;
                    maxWeightNumericUpDown.Enabled = false;
                    maxVolumeNumericUpDown.Enabled = false;
                    break;
                case "Van":
                    maxPassengersNumericUpDown.Enabled = true;
                    maxWeightNumericUpDown.Enabled = true;
                    maxVolumeNumericUpDown.Enabled = true;
                    break;
                case "Truck":
                    maxPassengersNumericUpDown.Enabled = false;
                    maxWeightNumericUpDown.Enabled = true;
                    maxVolumeNumericUpDown.Enabled = true;
                    break;
                default:
                    maxPassengersNumericUpDown.Enabled = false;
                    maxWeightNumericUpDown.Enabled = false;
                    maxVolumeNumericUpDown.Enabled = false;
                    break;
            }
        }

        private bool IsAllValid()
        {
            if (!IsTypeValid())
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, "Select a type of vehicle.");
                return false;
            }
            else
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, string.Empty);
            }

            if (IsLicensePlateEmpty())
            {
                licensePlateErrorProvider.SetError(licensePlateTextBox, "License Plate is required.");
                return false;
            }
            else if (IsLicensePlateUsed())
            {
                licensePlateErrorProvider.SetError(licensePlateTextBox, "License Plate already used.");
                return false;
            }
            else
            {
                licensePlateErrorProvider.Clear();
            }

            if (IsTotalKmEmpty())
            {
                totalKmErorProvider.SetError(totalKmNumericUpDown, "Travelled distance is required.");
                return false;
            }
            else
            {
                totalKmErorProvider.Clear();
            }

            if (IsPriceEmpty())
            {
                priceErrorProvider.SetError(priceNumericUpDown, "Price per km is required.");
                return false;
            }
            else
            {
                priceErrorProvider.Clear();
            }

            if (IsEmpty())
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, "Add passangers or load details.");
                return false;
            }
            else if (IsConflict())
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, "Conflict between passangers and load details.");
                return false;
            }
            else if (IsWeightButNoVolume())
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, "Volume is required.");
                return false;
            }
            else if (IsVolumeButNoWeight())
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, "Weight is required.");
                return false;
            }

            return true;
        }

        private bool IsEmpty()
        {
            return maxPassengersNumericUpDown.Value <= 0 && maxWeightNumericUpDown.Value <= 0 && maxVolumeNumericUpDown.Value <= 0;
        }
        private bool IsConflict()
        {
            return maxPassengersNumericUpDown.Value > 0 && (maxWeightNumericUpDown.Value > 0 || maxVolumeNumericUpDown.Value > 0);
        }

        private bool IsWeightButNoVolume()
        {
            return maxPassengersNumericUpDown.Value <= 0 && (maxWeightNumericUpDown.Value > 0 && maxVolumeNumericUpDown.Value <= 0);
        }

        private bool IsVolumeButNoWeight()
        {
            return maxPassengersNumericUpDown.Value <= 0 && (maxWeightNumericUpDown.Value <= 0 && maxVolumeNumericUpDown.Value > 0);
        }

        private void typeOfVehicleComboBox_Validating(object sender, CancelEventArgs e)
        {
            if (!IsTypeValid())
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, "Select a type of vehicle.");
            }
            else
            {
                typeOfVehicleErrorProvider.SetError(typeOfVehicleComboBox, string.Empty);
            }
        }

        private bool IsTypeValid()
        {
            return ((typeOfVehicleComboBox.SelectedItem != null) &&
                (!typeOfVehicleComboBox.SelectedItem.ToString().Equals("None")));
        }

        private void licensePlateTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (IsLicensePlateEmpty())
            {
                licensePlateErrorProvider.SetError(licensePlateTextBox, "License Plate is required.");
            }
            else if (IsLicensePlateUsed())
            {
                licensePlateErrorProvider.SetError(licensePlateTextBox, "License Plate already used.");
            }
            else
            {
                licensePlateErrorProvider.Clear();
            }
        }

        private bool IsLicensePlateEmpty()
        {
            return (licensePlateTextBox.Text.Length <= 0);
        }

        private bool IsLicensePlateUsed()
        {
            return SqliteDataAccess.LicensePlateExist(licensePlateTextBox.Text);
        }

        private void totalKmNumericUpDown_Validating(object sender, CancelEventArgs e)
        {
            if (IsTotalKmEmpty())
            {
                totalKmErorProvider.SetError(totalKmNumericUpDown, "Travelled distance is required.");
            }
            else
            {
                totalKmErorProvider.Clear();
            }
        }

        private bool IsTotalKmEmpty()
        {
            return (totalKmNumericUpDown.Value <= 0);
        }

        private void priceNumericUpDown_Validating(object sender, CancelEventArgs e)
        {
            if (IsPriceEmpty())
            {
                priceErrorProvider.SetError(priceNumericUpDown, "Price per km is required.");
            }
            else
            {
                priceErrorProvider.Clear();
            }
        }

        private bool IsPriceEmpty()
        {
            return (priceNumericUpDown.Value <= 0);
        }

        private void NewVehicleForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _frm.Enabled = true;
            _frm.Refresh();
        }
    }
}
