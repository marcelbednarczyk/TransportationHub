﻿
namespace TransportationHub
{
    partial class FinishRideForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label distanceLabel;
            System.Windows.Forms.Label iDLabel;
            System.Windows.Forms.Label peopleLabel;
            System.Windows.Forms.Label priceLabel;
            System.Windows.Forms.Label startPriceLabel;
            System.Windows.Forms.Label startTimeLabel;
            System.Windows.Forms.Label statusLabel;
            System.Windows.Forms.Label vehicleLicensePlateLabel;
            System.Windows.Forms.Label volumeLabel;
            System.Windows.Forms.Label weightLabel;
            this.distanceLabel1 = new System.Windows.Forms.Label();
            this.iDLabel1 = new System.Windows.Forms.Label();
            this.peopleLabel1 = new System.Windows.Forms.Label();
            this.priceLabel1 = new System.Windows.Forms.Label();
            this.startPriceLabel1 = new System.Windows.Forms.Label();
            this.startTimeLabel1 = new System.Windows.Forms.Label();
            this.statusLabel1 = new System.Windows.Forms.Label();
            this.vehicleLicensePlateLabel1 = new System.Windows.Forms.Label();
            this.volumeLabel1 = new System.Windows.Forms.Label();
            this.weightLabel1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.rideBindingSource = new System.Windows.Forms.BindingSource(this.components);
            distanceLabel = new System.Windows.Forms.Label();
            iDLabel = new System.Windows.Forms.Label();
            peopleLabel = new System.Windows.Forms.Label();
            priceLabel = new System.Windows.Forms.Label();
            startPriceLabel = new System.Windows.Forms.Label();
            startTimeLabel = new System.Windows.Forms.Label();
            statusLabel = new System.Windows.Forms.Label();
            vehicleLicensePlateLabel = new System.Windows.Forms.Label();
            volumeLabel = new System.Windows.Forms.Label();
            weightLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rideBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // distanceLabel
            // 
            distanceLabel.AutoSize = true;
            distanceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            distanceLabel.Location = new System.Drawing.Point(113, 39);
            distanceLabel.Name = "distanceLabel";
            distanceLabel.Size = new System.Drawing.Size(81, 20);
            distanceLabel.TabIndex = 1;
            distanceLabel.Text = "Distance:";
            // 
            // iDLabel
            // 
            iDLabel.AutoSize = true;
            iDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            iDLabel.Location = new System.Drawing.Point(163, 9);
            iDLabel.Name = "iDLabel";
            iDLabel.Size = new System.Drawing.Size(31, 20);
            iDLabel.TabIndex = 3;
            iDLabel.Text = "ID:";
            // 
            // peopleLabel
            // 
            peopleLabel.AutoSize = true;
            peopleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            peopleLabel.Location = new System.Drawing.Point(129, 189);
            peopleLabel.Name = "peopleLabel";
            peopleLabel.Size = new System.Drawing.Size(65, 20);
            peopleLabel.TabIndex = 5;
            peopleLabel.Text = "People:";
            // 
            // priceLabel
            // 
            priceLabel.AutoSize = true;
            priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            priceLabel.Location = new System.Drawing.Point(141, 99);
            priceLabel.Name = "priceLabel";
            priceLabel.Size = new System.Drawing.Size(53, 20);
            priceLabel.TabIndex = 7;
            priceLabel.Text = "Price:";
            // 
            // startPriceLabel
            // 
            startPriceLabel.AutoSize = true;
            startPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            startPriceLabel.Location = new System.Drawing.Point(100, 129);
            startPriceLabel.Name = "startPriceLabel";
            startPriceLabel.Size = new System.Drawing.Size(94, 20);
            startPriceLabel.TabIndex = 9;
            startPriceLabel.Text = "Start Price:";
            // 
            // startTimeLabel
            // 
            startTimeLabel.AutoSize = true;
            startTimeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            startTimeLabel.Location = new System.Drawing.Point(102, 159);
            startTimeLabel.Name = "startTimeLabel";
            startTimeLabel.Size = new System.Drawing.Size(92, 20);
            startTimeLabel.TabIndex = 11;
            startTimeLabel.Text = "Start Time:";
            // 
            // statusLabel
            // 
            statusLabel.AutoSize = true;
            statusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            statusLabel.Location = new System.Drawing.Point(132, 279);
            statusLabel.Name = "statusLabel";
            statusLabel.Size = new System.Drawing.Size(62, 20);
            statusLabel.TabIndex = 13;
            statusLabel.Text = "Status:";
            // 
            // vehicleLicensePlateLabel
            // 
            vehicleLicensePlateLabel.AutoSize = true;
            vehicleLicensePlateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            vehicleLicensePlateLabel.Location = new System.Drawing.Point(18, 69);
            vehicleLicensePlateLabel.Name = "vehicleLicensePlateLabel";
            vehicleLicensePlateLabel.Size = new System.Drawing.Size(176, 20);
            vehicleLicensePlateLabel.TabIndex = 15;
            vehicleLicensePlateLabel.Text = "Vehicle License Plate:";
            // 
            // volumeLabel
            // 
            volumeLabel.AutoSize = true;
            volumeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            volumeLabel.Location = new System.Drawing.Point(124, 219);
            volumeLabel.Name = "volumeLabel";
            volumeLabel.Size = new System.Drawing.Size(70, 20);
            volumeLabel.TabIndex = 17;
            volumeLabel.Text = "Volume:";
            // 
            // weightLabel
            // 
            weightLabel.AutoSize = true;
            weightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            weightLabel.Location = new System.Drawing.Point(128, 249);
            weightLabel.Name = "weightLabel";
            weightLabel.Size = new System.Drawing.Size(66, 20);
            weightLabel.TabIndex = 19;
            weightLabel.Text = "Weight:";
            // 
            // distanceLabel1
            // 
            this.distanceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "Distance", true));
            this.distanceLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.distanceLabel1.Location = new System.Drawing.Point(239, 39);
            this.distanceLabel1.Name = "distanceLabel1";
            this.distanceLabel1.Size = new System.Drawing.Size(158, 23);
            this.distanceLabel1.TabIndex = 2;
            this.distanceLabel1.Text = "label1";
            // 
            // iDLabel1
            // 
            this.iDLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "ID", true));
            this.iDLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.iDLabel1.Location = new System.Drawing.Point(239, 9);
            this.iDLabel1.Name = "iDLabel1";
            this.iDLabel1.Size = new System.Drawing.Size(158, 23);
            this.iDLabel1.TabIndex = 4;
            this.iDLabel1.Text = "label1";
            // 
            // peopleLabel1
            // 
            this.peopleLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "People", true));
            this.peopleLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.peopleLabel1.Location = new System.Drawing.Point(239, 189);
            this.peopleLabel1.Name = "peopleLabel1";
            this.peopleLabel1.Size = new System.Drawing.Size(158, 23);
            this.peopleLabel1.TabIndex = 6;
            this.peopleLabel1.Text = "label1";
            // 
            // priceLabel1
            // 
            this.priceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "Price", true));
            this.priceLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.priceLabel1.Location = new System.Drawing.Point(239, 99);
            this.priceLabel1.Name = "priceLabel1";
            this.priceLabel1.Size = new System.Drawing.Size(158, 23);
            this.priceLabel1.TabIndex = 8;
            this.priceLabel1.Text = "label1";
            // 
            // startPriceLabel1
            // 
            this.startPriceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "StartPrice", true));
            this.startPriceLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.startPriceLabel1.Location = new System.Drawing.Point(239, 129);
            this.startPriceLabel1.Name = "startPriceLabel1";
            this.startPriceLabel1.Size = new System.Drawing.Size(158, 23);
            this.startPriceLabel1.TabIndex = 10;
            this.startPriceLabel1.Text = "label1";
            // 
            // startTimeLabel1
            // 
            this.startTimeLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "StartTime", true));
            this.startTimeLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.startTimeLabel1.Location = new System.Drawing.Point(239, 159);
            this.startTimeLabel1.Name = "startTimeLabel1";
            this.startTimeLabel1.Size = new System.Drawing.Size(158, 23);
            this.startTimeLabel1.TabIndex = 12;
            this.startTimeLabel1.Text = "label1";
            // 
            // statusLabel1
            // 
            this.statusLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "Status", true));
            this.statusLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.statusLabel1.Location = new System.Drawing.Point(239, 279);
            this.statusLabel1.Name = "statusLabel1";
            this.statusLabel1.Size = new System.Drawing.Size(158, 23);
            this.statusLabel1.TabIndex = 14;
            this.statusLabel1.Text = "label1";
            // 
            // vehicleLicensePlateLabel1
            // 
            this.vehicleLicensePlateLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "VehicleLicensePlate", true));
            this.vehicleLicensePlateLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.vehicleLicensePlateLabel1.Location = new System.Drawing.Point(239, 69);
            this.vehicleLicensePlateLabel1.Name = "vehicleLicensePlateLabel1";
            this.vehicleLicensePlateLabel1.Size = new System.Drawing.Size(158, 23);
            this.vehicleLicensePlateLabel1.TabIndex = 16;
            this.vehicleLicensePlateLabel1.Text = "label1";
            // 
            // volumeLabel1
            // 
            this.volumeLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "Volume", true));
            this.volumeLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.volumeLabel1.Location = new System.Drawing.Point(239, 219);
            this.volumeLabel1.Name = "volumeLabel1";
            this.volumeLabel1.Size = new System.Drawing.Size(158, 23);
            this.volumeLabel1.TabIndex = 18;
            this.volumeLabel1.Text = "label1";
            // 
            // weightLabel1
            // 
            this.weightLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rideBindingSource, "Weight", true));
            this.weightLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.weightLabel1.Location = new System.Drawing.Point(239, 249);
            this.weightLabel1.Name = "weightLabel1";
            this.weightLabel1.Size = new System.Drawing.Size(158, 23);
            this.weightLabel1.TabIndex = 20;
            this.weightLabel1.Text = "label1";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(243, 318);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 31);
            this.button1.TabIndex = 21;
            this.button1.Text = "Finish";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rideBindingSource
            // 
            this.rideBindingSource.DataSource = typeof(TransportationHubLibrary.Ride);
            // 
            // FinishRideForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 364);
            this.Controls.Add(this.button1);
            this.Controls.Add(distanceLabel);
            this.Controls.Add(this.distanceLabel1);
            this.Controls.Add(iDLabel);
            this.Controls.Add(this.iDLabel1);
            this.Controls.Add(peopleLabel);
            this.Controls.Add(this.peopleLabel1);
            this.Controls.Add(priceLabel);
            this.Controls.Add(this.priceLabel1);
            this.Controls.Add(startPriceLabel);
            this.Controls.Add(this.startPriceLabel1);
            this.Controls.Add(startTimeLabel);
            this.Controls.Add(this.startTimeLabel1);
            this.Controls.Add(statusLabel);
            this.Controls.Add(this.statusLabel1);
            this.Controls.Add(vehicleLicensePlateLabel);
            this.Controls.Add(this.vehicleLicensePlateLabel1);
            this.Controls.Add(volumeLabel);
            this.Controls.Add(this.volumeLabel1);
            this.Controls.Add(weightLabel);
            this.Controls.Add(this.weightLabel1);
            this.Name = "FinishRideForm";
            this.Text = "FinishRideForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FinishRideForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.rideBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource rideBindingSource;
        private System.Windows.Forms.Label distanceLabel1;
        private System.Windows.Forms.Label iDLabel1;
        private System.Windows.Forms.Label peopleLabel1;
        private System.Windows.Forms.Label priceLabel1;
        private System.Windows.Forms.Label startPriceLabel1;
        private System.Windows.Forms.Label startTimeLabel1;
        private System.Windows.Forms.Label statusLabel1;
        private System.Windows.Forms.Label vehicleLicensePlateLabel1;
        private System.Windows.Forms.Label volumeLabel1;
        private System.Windows.Forms.Label weightLabel1;
        private System.Windows.Forms.Button button1;
    }
}