﻿
namespace Transportation_Hub
{
    partial class NewVehicleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.typeOfVehicleLabel = new System.Windows.Forms.Label();
            this.typeOfVehicleComboBox = new System.Windows.Forms.ComboBox();
            this.typeOfVehicleErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.makeAndModelLabel = new System.Windows.Forms.Label();
            this.makeAndModelTextBox = new System.Windows.Forms.TextBox();
            this.licensePlateTextBox = new System.Windows.Forms.TextBox();
            this.licensePlateLabel = new System.Windows.Forms.Label();
            this.priceErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.licensePlateErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.totalKmErorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.gasUsageLabel = new System.Windows.Forms.Label();
            this.gasUsageNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.priceNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.priceLabel = new System.Windows.Forms.Label();
            this.totalKmNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.totalKmLabel = new System.Windows.Forms.Label();
            this.gasUsageErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.saveButton = new System.Windows.Forms.Button();
            this.maxPassengersNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.maxPassangersLabel = new System.Windows.Forms.Label();
            this.maxWeightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.maxWeightLabel = new System.Windows.Forms.Label();
            this.maxVolumeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.maxVolumeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.typeOfVehicleErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.licensePlateErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalKmErorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gasUsageNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalKmNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gasUsageErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPassengersNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxWeightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxVolumeNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // typeOfVehicleLabel
            // 
            this.typeOfVehicleLabel.AutoSize = true;
            this.typeOfVehicleLabel.Location = new System.Drawing.Point(61, 32);
            this.typeOfVehicleLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.typeOfVehicleLabel.Name = "typeOfVehicleLabel";
            this.typeOfVehicleLabel.Size = new System.Drawing.Size(124, 20);
            this.typeOfVehicleLabel.TabIndex = 0;
            this.typeOfVehicleLabel.Text = "Type of Vehicle";
            // 
            // typeOfVehicleComboBox
            // 
            this.typeOfVehicleComboBox.FormattingEnabled = true;
            this.typeOfVehicleComboBox.Items.AddRange(new object[] {
            "None",
            "Car",
            "Van",
            "Truck"});
            this.typeOfVehicleComboBox.Location = new System.Drawing.Point(207, 29);
            this.typeOfVehicleComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.typeOfVehicleComboBox.Name = "typeOfVehicleComboBox";
            this.typeOfVehicleComboBox.Size = new System.Drawing.Size(221, 28);
            this.typeOfVehicleComboBox.TabIndex = 1;
            this.typeOfVehicleComboBox.SelectedValueChanged += new System.EventHandler(this.typeOfVehicleComboBox_SelectedValueChanged);
            this.typeOfVehicleComboBox.Validating += new System.ComponentModel.CancelEventHandler(this.typeOfVehicleComboBox_Validating);
            // 
            // typeOfVehicleErrorProvider
            // 
            this.typeOfVehicleErrorProvider.ContainerControl = this;
            // 
            // makeAndModelLabel
            // 
            this.makeAndModelLabel.AutoSize = true;
            this.makeAndModelLabel.Location = new System.Drawing.Point(54, 67);
            this.makeAndModelLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.makeAndModelLabel.Name = "makeAndModelLabel";
            this.makeAndModelLabel.Size = new System.Drawing.Size(131, 20);
            this.makeAndModelLabel.TabIndex = 2;
            this.makeAndModelLabel.Text = "Make and Model";
            // 
            // makeAndModelTextBox
            // 
            this.makeAndModelTextBox.Location = new System.Drawing.Point(207, 64);
            this.makeAndModelTextBox.Name = "makeAndModelTextBox";
            this.makeAndModelTextBox.Size = new System.Drawing.Size(221, 26);
            this.makeAndModelTextBox.TabIndex = 3;
            // 
            // licensePlateTextBox
            // 
            this.licensePlateTextBox.Location = new System.Drawing.Point(207, 96);
            this.licensePlateTextBox.Name = "licensePlateTextBox";
            this.licensePlateTextBox.Size = new System.Drawing.Size(221, 26);
            this.licensePlateTextBox.TabIndex = 5;
            this.licensePlateTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.licensePlateTextBox_Validating);
            // 
            // licensePlateLabel
            // 
            this.licensePlateLabel.AutoSize = true;
            this.licensePlateLabel.Location = new System.Drawing.Point(74, 99);
            this.licensePlateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.licensePlateLabel.Name = "licensePlateLabel";
            this.licensePlateLabel.Size = new System.Drawing.Size(111, 20);
            this.licensePlateLabel.TabIndex = 4;
            this.licensePlateLabel.Text = "License Plate";
            // 
            // priceErrorProvider
            // 
            this.priceErrorProvider.ContainerControl = this;
            // 
            // licensePlateErrorProvider
            // 
            this.licensePlateErrorProvider.ContainerControl = this;
            // 
            // totalKmErorProvider
            // 
            this.totalKmErorProvider.ContainerControl = this;
            // 
            // gasUsageLabel
            // 
            this.gasUsageLabel.AutoSize = true;
            this.gasUsageLabel.Location = new System.Drawing.Point(19, 130);
            this.gasUsageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gasUsageLabel.Name = "gasUsageLabel";
            this.gasUsageLabel.Size = new System.Drawing.Size(166, 20);
            this.gasUsageLabel.TabIndex = 6;
            this.gasUsageLabel.Text = "Gas Usage [l/100km]";
            // 
            // gasUsageNumericUpDown
            // 
            this.gasUsageNumericUpDown.DecimalPlaces = 1;
            this.gasUsageNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.gasUsageNumericUpDown.Location = new System.Drawing.Point(207, 128);
            this.gasUsageNumericUpDown.Name = "gasUsageNumericUpDown";
            this.gasUsageNumericUpDown.Size = new System.Drawing.Size(221, 26);
            this.gasUsageNumericUpDown.TabIndex = 7;
            // 
            // priceNumericUpDown
            // 
            this.priceNumericUpDown.DecimalPlaces = 2;
            this.priceNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.priceNumericUpDown.Location = new System.Drawing.Point(207, 160);
            this.priceNumericUpDown.Name = "priceNumericUpDown";
            this.priceNumericUpDown.Size = new System.Drawing.Size(221, 26);
            this.priceNumericUpDown.TabIndex = 9;
            this.priceNumericUpDown.Validating += new System.ComponentModel.CancelEventHandler(this.priceNumericUpDown_Validating);
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(86, 162);
            this.priceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(99, 20);
            this.priceLabel.TabIndex = 8;
            this.priceLabel.Text = "Price [€/km]";
            // 
            // totalKmNumericUpDown
            // 
            this.totalKmNumericUpDown.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.totalKmNumericUpDown.Location = new System.Drawing.Point(207, 192);
            this.totalKmNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.totalKmNumericUpDown.Name = "totalKmNumericUpDown";
            this.totalKmNumericUpDown.Size = new System.Drawing.Size(221, 26);
            this.totalKmNumericUpDown.TabIndex = 11;
            this.totalKmNumericUpDown.Validating += new System.ComponentModel.CancelEventHandler(this.totalKmNumericUpDown_Validating);
            // 
            // totalKmLabel
            // 
            this.totalKmLabel.AutoSize = true;
            this.totalKmLabel.Location = new System.Drawing.Point(39, 194);
            this.totalKmLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.totalKmLabel.Name = "totalKmLabel";
            this.totalKmLabel.Size = new System.Drawing.Size(146, 20);
            this.totalKmLabel.TabIndex = 10;
            this.totalKmLabel.Text = "Total mileage [km]";
            // 
            // gasUsageErrorProvider
            // 
            this.gasUsageErrorProvider.ContainerControl = this;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(318, 324);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(110, 29);
            this.saveButton.TabIndex = 12;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // maxPassengersNumericUpDown
            // 
            this.maxPassengersNumericUpDown.Enabled = false;
            this.maxPassengersNumericUpDown.Location = new System.Drawing.Point(207, 224);
            this.maxPassengersNumericUpDown.Name = "maxPassengersNumericUpDown";
            this.maxPassengersNumericUpDown.Size = new System.Drawing.Size(221, 26);
            this.maxPassengersNumericUpDown.TabIndex = 14;
            // 
            // maxPassangersLabel
            // 
            this.maxPassangersLabel.AutoSize = true;
            this.maxPassangersLabel.Location = new System.Drawing.Point(44, 226);
            this.maxPassangersLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.maxPassangersLabel.Name = "maxPassangersLabel";
            this.maxPassangersLabel.Size = new System.Drawing.Size(138, 20);
            this.maxPassangersLabel.TabIndex = 13;
            this.maxPassangersLabel.Text = "Max. Passengers";
            // 
            // maxWeightNumericUpDown
            // 
            this.maxWeightNumericUpDown.Enabled = false;
            this.maxWeightNumericUpDown.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.maxWeightNumericUpDown.Location = new System.Drawing.Point(207, 256);
            this.maxWeightNumericUpDown.Maximum = new decimal(new int[] {
            500000,
            0,
            0,
            0});
            this.maxWeightNumericUpDown.Name = "maxWeightNumericUpDown";
            this.maxWeightNumericUpDown.Size = new System.Drawing.Size(221, 26);
            this.maxWeightNumericUpDown.TabIndex = 16;
            // 
            // maxWeightLabel
            // 
            this.maxWeightLabel.AutoSize = true;
            this.maxWeightLabel.Location = new System.Drawing.Point(49, 258);
            this.maxWeightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.maxWeightLabel.Name = "maxWeightLabel";
            this.maxWeightLabel.Size = new System.Drawing.Size(133, 20);
            this.maxWeightLabel.TabIndex = 15;
            this.maxWeightLabel.Text = "Max. Weight [kg]";
            // 
            // maxVolumeNumericUpDown
            // 
            this.maxVolumeNumericUpDown.DecimalPlaces = 2;
            this.maxVolumeNumericUpDown.Enabled = false;
            this.maxVolumeNumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.maxVolumeNumericUpDown.Location = new System.Drawing.Point(207, 288);
            this.maxVolumeNumericUpDown.Name = "maxVolumeNumericUpDown";
            this.maxVolumeNumericUpDown.Size = new System.Drawing.Size(221, 26);
            this.maxVolumeNumericUpDown.TabIndex = 18;
            // 
            // maxVolumeLabel
            // 
            this.maxVolumeLabel.AutoSize = true;
            this.maxVolumeLabel.Location = new System.Drawing.Point(39, 290);
            this.maxVolumeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.maxVolumeLabel.Name = "maxVolumeLabel";
            this.maxVolumeLabel.Size = new System.Drawing.Size(143, 20);
            this.maxVolumeLabel.TabIndex = 17;
            this.maxVolumeLabel.Text = "Max. Volume [m3]";
            // 
            // NewVehicleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 365);
            this.Controls.Add(this.maxVolumeNumericUpDown);
            this.Controls.Add(this.maxVolumeLabel);
            this.Controls.Add(this.maxWeightNumericUpDown);
            this.Controls.Add(this.maxWeightLabel);
            this.Controls.Add(this.maxPassengersNumericUpDown);
            this.Controls.Add(this.maxPassangersLabel);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.totalKmNumericUpDown);
            this.Controls.Add(this.totalKmLabel);
            this.Controls.Add(this.priceNumericUpDown);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.gasUsageNumericUpDown);
            this.Controls.Add(this.gasUsageLabel);
            this.Controls.Add(this.licensePlateTextBox);
            this.Controls.Add(this.licensePlateLabel);
            this.Controls.Add(this.makeAndModelTextBox);
            this.Controls.Add(this.makeAndModelLabel);
            this.Controls.Add(this.typeOfVehicleComboBox);
            this.Controls.Add(this.typeOfVehicleLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "NewVehicleForm";
            this.Text = "Add New Vehicle";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NewVehicleForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.typeOfVehicleErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.licensePlateErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalKmErorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gasUsageNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.priceNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totalKmNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gasUsageErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPassengersNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxWeightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxVolumeNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label typeOfVehicleLabel;
        private System.Windows.Forms.ComboBox typeOfVehicleComboBox;
        private System.Windows.Forms.ErrorProvider typeOfVehicleErrorProvider;
        private System.Windows.Forms.TextBox makeAndModelTextBox;
        private System.Windows.Forms.Label makeAndModelLabel;
        private System.Windows.Forms.TextBox licensePlateTextBox;
        private System.Windows.Forms.Label licensePlateLabel;
        private System.Windows.Forms.ErrorProvider priceErrorProvider;
        private System.Windows.Forms.ErrorProvider licensePlateErrorProvider;
        private System.Windows.Forms.ErrorProvider totalKmErorProvider;
        private System.Windows.Forms.NumericUpDown gasUsageNumericUpDown;
        private System.Windows.Forms.Label gasUsageLabel;
        private System.Windows.Forms.NumericUpDown totalKmNumericUpDown;
        private System.Windows.Forms.Label totalKmLabel;
        private System.Windows.Forms.NumericUpDown priceNumericUpDown;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.ErrorProvider gasUsageErrorProvider;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.NumericUpDown maxVolumeNumericUpDown;
        private System.Windows.Forms.Label maxVolumeLabel;
        private System.Windows.Forms.NumericUpDown maxWeightNumericUpDown;
        private System.Windows.Forms.Label maxWeightLabel;
        private System.Windows.Forms.NumericUpDown maxPassengersNumericUpDown;
        private System.Windows.Forms.Label maxPassangersLabel;
    }
}