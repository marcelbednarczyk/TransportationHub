﻿using System;
using System.Windows.Forms;
using Transportation_Hub;
using TransportationHubLibrary;

namespace TransportationHub
{
    public partial class FinishRideForm : Form
    {
        private Ride _ride;
        private Form _frm;
        public FinishRideForm(TransportationHubForm senderForm, int ID)
        {
            _frm = senderForm;
            InitializeComponent();
            _ride = SqliteDataAccess.LoadRide(ID);
            rideBindingSource.DataSource = _ride;
            if(_ride.Status == "Finished")
            {
                button1.Text = "Close";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _ride.Finish();
            SqliteDataAccess.UpdateRide(_ride);
            var vehicle = SqliteDataAccess.LoadVehicle(_ride.VehicleLicensePlate);
            vehicle.TotalKilometres += _ride.Distance;
            if (vehicle.MaxWeight <= 0)
            {
                SqliteDataAccess.UpdateVehicle(new Car(vehicle));
            }
            else if (vehicle.MaxPassengers > 0)
            {
                SqliteDataAccess.UpdateVehicle(vehicle);
            }
            else
            {
                SqliteDataAccess.UpdateVehicle(new Truck(vehicle));
            }
            
            Close();
        }

        private void FinishRideForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _frm.Enabled = true;
            _frm.Refresh();
        }
    }
}
