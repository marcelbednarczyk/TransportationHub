﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Windows.Forms;
using Transportation_Hub;
using TransportationHubLibrary;

namespace TransportationHub
{
    public partial class NewRideForm : Form
    {
        private Form _frm;

        public NewRideForm(TransportationHubForm senderForm)
        {
            _frm = senderForm;
            InitializeComponent();
            Text = "Add New Ride";
            rideBindingSource.DataSource = new Ride();
        }

        private void NewRideForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _frm.Enabled = true;
            _frm.Refresh();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            rideBindingSource.EndEdit();
            if (rideBindingSource.Current is Ride ride)
            {
                if (ride.IsValid)
                {
                    if (IsEmpty())
                    {
                        errorProvider.Clear();
                        errorProvider.SetError(peopleNumericUpDown1, "Passangers or load are required.");
                        errorProvider.SetError(volumeNumericUpDown1, "Passangers or load are required.");
                        errorProvider.SetError(weightNumericUpDown1, "Passangers or load are required.");
                    }
                    else if (IsConflict())
                    {
                        errorProvider.Clear();
                        errorProvider.SetError(peopleNumericUpDown1, "You cannot transport passengers and load at the same time.");
                    }
                    else if (IsWeightButNoVolume())
                    {
                        errorProvider.Clear();
                        errorProvider.SetError(volumeNumericUpDown1, "Volume is required.");
                    }
                    else if (IsVolumeButNoWeight())
                    {
                        errorProvider.Clear();
                        errorProvider.SetError(weightNumericUpDown1, "Weight is required.");
                    }
                    else
                    {
                        errorProvider.Clear();
                        var vehicle = FindVehicle();
                        if (vehicle != null)
                        {
                            SaveRide(vehicle);
                            Close();
                        } 
                        else
                        {
                            MessageNoAvailableVehicles();
                        }
                    }
                }
            }
        }

        private bool IsEmpty()
        {
            return peopleNumericUpDown1.Value <= 0 && weightNumericUpDown1.Value <= 0 && volumeNumericUpDown1.Value <= 0;
        }
        private bool IsConflict()
        {
            return peopleNumericUpDown1.Value > 0 && (weightNumericUpDown1.Value > 0 || volumeNumericUpDown1.Value > 0);
        }

        private bool IsWeightButNoVolume()
        {
            return peopleNumericUpDown1.Value <= 0 && (weightNumericUpDown1.Value > 0 && volumeNumericUpDown1.Value <= 0);
        }

        private bool IsVolumeButNoWeight()
        {
            return peopleNumericUpDown1.Value <= 0 && (weightNumericUpDown1.Value <= 0 && volumeNumericUpDown1.Value > 0);
        }

        private string FindVehicle()
        {
            return SqliteDataAccess.FindVehicle((int)peopleNumericUpDown1.Value, (int)weightNumericUpDown1.Value, (double)volumeNumericUpDown1.Value);
        }

        private void SaveRide(string vehicle)
        {
            Ride ride = new Ride(vehicle,
                                (int)distanceNumericUpDown1.Value,
                                (int)peopleNumericUpDown1.Value,
                                (double)volumeNumericUpDown1.Value,
                                (int)weightNumericUpDown1.Value);
            SqliteDataAccess.SaveRide(ride);
        }

        private void MessageNoAvailableVehicles()
        {
            MessageBox.Show("There are no available vehicles that could meet your criteria.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
