﻿
namespace Transportation_Hub
{
    partial class TransportationHubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_add_new = new System.Windows.Forms.Button();
            this.refreshButton = new System.Windows.Forms.Button();
            this.listAvailVehiclesListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.newRideButton = new System.Windows.Forms.Button();
            this.ridesListView = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.iVehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.listVehiclesListView = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.completedRidesListView = new System.Windows.Forms.ListView();
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.iVehicleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_add_new
            // 
            this.btn_add_new.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_add_new.Location = new System.Drawing.Point(1205, 14);
            this.btn_add_new.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_add_new.Name = "btn_add_new";
            this.btn_add_new.Size = new System.Drawing.Size(127, 28);
            this.btn_add_new.TabIndex = 0;
            this.btn_add_new.Text = "New Vehicle";
            this.btn_add_new.UseVisualStyleBackColor = true;
            this.btn_add_new.Click += new System.EventHandler(this.btn_add_new_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.refreshButton.Location = new System.Drawing.Point(676, 52);
            this.refreshButton.Margin = new System.Windows.Forms.Padding(4);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(104, 28);
            this.refreshButton.TabIndex = 2;
            this.refreshButton.Text = "Refresh All";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // listAvailVehiclesListView
            // 
            this.listAvailVehiclesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listAvailVehiclesListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listAvailVehiclesListView.GridLines = true;
            this.listAvailVehiclesListView.HideSelection = false;
            this.listAvailVehiclesListView.Location = new System.Drawing.Point(812, 52);
            this.listAvailVehiclesListView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listAvailVehiclesListView.Name = "listAvailVehiclesListView";
            this.listAvailVehiclesListView.Size = new System.Drawing.Size(520, 362);
            this.listAvailVehiclesListView.TabIndex = 5;
            this.listAvailVehiclesListView.UseCompatibleStateImageBehavior = false;
            this.listAvailVehiclesListView.View = System.Windows.Forms.View.Details;
            this.listAvailVehiclesListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listVehiclesListView_ColumnClick);
            this.listAvailVehiclesListView.ItemActivate += new System.EventHandler(this.listVehiclesListView_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "License Plate";
            this.columnHeader1.Width = 104;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Make And Model";
            this.columnHeader2.Width = 134;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Price [€/km]";
            this.columnHeader3.Width = 86;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Type";
            // 
            // newRideButton
            // 
            this.newRideButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.newRideButton.Location = new System.Drawing.Point(529, 14);
            this.newRideButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.newRideButton.Name = "newRideButton";
            this.newRideButton.Size = new System.Drawing.Size(111, 28);
            this.newRideButton.TabIndex = 6;
            this.newRideButton.Text = "New Ride";
            this.newRideButton.UseVisualStyleBackColor = true;
            this.newRideButton.Click += new System.EventHandler(this.newRideButton_Click);
            // 
            // ridesListView
            // 
            this.ridesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader5,
            this.columnHeader7});
            this.ridesListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ridesListView.GridLines = true;
            this.ridesListView.HideSelection = false;
            this.ridesListView.Location = new System.Drawing.Point(15, 52);
            this.ridesListView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ridesListView.Name = "ridesListView";
            this.ridesListView.Size = new System.Drawing.Size(625, 362);
            this.ridesListView.TabIndex = 7;
            this.ridesListView.UseCompatibleStateImageBehavior = false;
            this.ridesListView.View = System.Windows.Forms.View.Details;
            this.ridesListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.ridesListView_ColumnClick);
            this.ridesListView.ItemActivate += new System.EventHandler(this.ridesListView_ItemActivate);
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "ID";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "License Plate";
            this.columnHeader12.Width = 114;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Start";
            this.columnHeader5.Width = 162;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Status";
            this.columnHeader7.Width = 91;
            // 
            // listVehiclesListView
            // 
            this.listVehiclesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader13,
            this.columnHeader14});
            this.listVehiclesListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listVehiclesListView.GridLines = true;
            this.listVehiclesListView.HideSelection = false;
            this.listVehiclesListView.Location = new System.Drawing.Point(812, 448);
            this.listVehiclesListView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listVehiclesListView.Name = "listVehiclesListView";
            this.listVehiclesListView.Size = new System.Drawing.Size(520, 338);
            this.listVehiclesListView.TabIndex = 8;
            this.listVehiclesListView.UseCompatibleStateImageBehavior = false;
            this.listVehiclesListView.View = System.Windows.Forms.View.Details;
            this.listVehiclesListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listVehiclesListView_ColumnClick_1);
            this.listVehiclesListView.ItemActivate += new System.EventHandler(this.listVehiclesListView_ItemActivate_1);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "License Plate";
            this.columnHeader9.Width = 104;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Make And Model";
            this.columnHeader10.Width = 134;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Price [€/km]";
            this.columnHeader13.Width = 86;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Type";
            // 
            // completedRidesListView
            // 
            this.completedRidesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader6,
            this.columnHeader18,
            this.columnHeader20,
            this.columnHeader19});
            this.completedRidesListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.completedRidesListView.GridLines = true;
            this.completedRidesListView.HideSelection = false;
            this.completedRidesListView.Location = new System.Drawing.Point(15, 448);
            this.completedRidesListView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.completedRidesListView.Name = "completedRidesListView";
            this.completedRidesListView.Size = new System.Drawing.Size(625, 338);
            this.completedRidesListView.TabIndex = 9;
            this.completedRidesListView.UseCompatibleStateImageBehavior = false;
            this.completedRidesListView.View = System.Windows.Forms.View.Details;
            this.completedRidesListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.completedRidesListView_ColumnClick);
            this.completedRidesListView.ItemActivate += new System.EventHandler(this.completedRidesListView_ItemActivate);
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "ID";
            this.columnHeader15.Width = 33;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "License Plate";
            this.columnHeader16.Width = 38;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Start";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "End";
            this.columnHeader6.Width = 52;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Distance";
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Price";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Status";
            this.columnHeader19.Width = 73;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(810, 420);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Vehicles currently busy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(810, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Available vehicles";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(12, 420);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Finished rides";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Rides in progress";
            // 
            // TransportationHubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 846);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.completedRidesListView);
            this.Controls.Add(this.listVehiclesListView);
            this.Controls.Add(this.ridesListView);
            this.Controls.Add(this.newRideButton);
            this.Controls.Add(this.listAvailVehiclesListView);
            this.Controls.Add(this.refreshButton);
            this.Controls.Add(this.btn_add_new);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "TransportationHubForm";
            this.Text = "Vehicles";
            this.Activated += new System.EventHandler(this.VehiclesForm_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.iVehicleBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_add_new;
        private System.Windows.Forms.BindingSource iVehicleBindingSource;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.ListView listAvailVehiclesListView;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button newRideButton;
        private System.Windows.Forms.ListView ridesListView;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ListView listVehiclesListView;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ListView completedRidesListView;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

